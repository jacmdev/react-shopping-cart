# About

Sample shopping cart project with basic functionality:

- Categories
- Product list
- Quick cart preview
- Confirmation modal
- Checkout with steps
    - Cart - product management
    - Shipping & payment - form with validation
    - Summary
    - Order confirmation

# Demo

https://jacmdev.com/react-shopping-cart/

# Installation and use

1. Clone repository
2. Install required packages

```
npm ci
```

3. Run project

- fake API server with sample data

```
npm run api
```

- app live server

```
npm start
```

# Libraries

- React - view layer
- React Router - navigation & routing
- Redux - state management
- Formik - form builder
- Yup - form validation
- Big.js - arithmetic
- Font Awesome - icons
- JSON Server - fake API
