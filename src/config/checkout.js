import { currency } from '../utils';

const config = {
  paymentMethods: [
    { value: '1', label: 'Payment #1' },
    { value: '2', label: 'Payment #2' },
    { value: '3', label: 'Payment #3' },
  ],
  shippingMethods: [
    { value: '1', label: 'Shipping #1', cost: 0, extra: <span className="text-light"> (Free)</span> },
    { value: '2', label: 'Shipping #2', cost: 5.5, extra: <span className="text-light"> ({currency(5.5)})</span> },
    { value: '3', label: 'Shipping #3', cost: 6.5, extra: <span className="text-light"> ({currency(6.5)})</span> },
  ],
  steps: {
    cart: { id: 1, name: 'Cart' },
    form: { id: 2, name: 'Shipping & Payment' },
    summary: { id: 3, name: 'Summary' },
  },
  stepsOrdered: ['cart', 'form', 'summary'],
};

export default config;
