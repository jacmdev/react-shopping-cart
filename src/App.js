import { Redirect, Route, Switch } from 'react-router-dom';
import Layout from './components/layout/Layout';
import CheckoutContextProvider from './context/checkout-context';
import Category from './pages/Category';
import Checkout from './pages/Checkout';
import CheckoutConfirmation from './pages/CheckoutConfirmation';
import PageNotFound from './pages/PageNotFound';

export default function App() {
  return (
    <Layout>
      <Switch>
        <Route path="/" exact>
          <Redirect to="/all-products" />
        </Route>
        <Route path="/all-products">
          <Category />
        </Route>
        <Route path="/category-:id">
          <Category />
        </Route>
        <Route path="/checkout">
          <CheckoutContextProvider>
            <Checkout />
          </CheckoutContextProvider>
        </Route>
        <Route path="/confirmation">
          <CheckoutConfirmation />
        </Route>
        <Route path="*">
          <PageNotFound />
        </Route>
      </Switch>
    </Layout>
  );
}
