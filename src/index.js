import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import ModalContextProvider from './context/modal-context';
import store from './store';
import App from './App';
import 'font-awesome/css/font-awesome.min.css';
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ModalContextProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ModalContextProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
