export const checkoutActions = {
  clear: '[checkout] clear',
  save: '[checkout] save',
  updateShipping: '[checkout] update shipping',
};

const initialState = {
  address: '',
  isDone: false,
  name: '',
  optional: false,
  payment: '',
  phone: '',
  shipping: '1',
};

export function checkoutReducer(state = initialState, action) {
  switch (action.type) {
    case checkoutActions.clear:
      return { ...initialState };
    case checkoutActions.save:
      return { ...action.payload };
    case checkoutActions.updateShipping:
      return { ...state, shipping: action.payload };
    default:
      return state;
  }
}
