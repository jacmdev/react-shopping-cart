import { combineReducers, createStore } from 'redux';
import { cartReducer } from './cart';
import { checkoutReducer } from './checkout';

const loadFromStorage = () => {
  const state = localStorage.getItem('state');
  if (state) {
    return JSON.parse(state);
  }
};

const saveToStorage = (state) => {
  const { cart } = state;
  localStorage.setItem('state', JSON.stringify({ cart }));
};

const rootReducer = combineReducers({ cart: cartReducer, checkout: checkoutReducer });
const store = createStore(rootReducer, loadFromStorage());

store.subscribe(() => saveToStorage(store.getState()));

export default store;
