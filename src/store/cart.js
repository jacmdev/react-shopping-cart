import Big from 'big.js';

export const cartActions = {
  addProduct: '[cart] add product',
  changeProductQuantity: '[cart] change product quantity',
  clear: '[cart] clear',
  removeProduct: '[cart] remove product',
};

const initialState = {
  products: [],
  totalPrice: 0,
  totalProducts: 0,
};

const getTotals = (products) => {
  let totalPrice = new Big(0);
  let totalProducts = 0;

  products.forEach((product) => {
    totalProducts += product.quantity;
    totalPrice = totalPrice.plus(new Big(product.price).times(product.quantity));
  });

  return { totalPrice: totalPrice.toNumber(), totalProducts };
};

export function cartReducer(state = initialState, action) {
  switch (action.type) {
    case cartActions.addProduct: {
      const productInCart = state.products.find((product) => product.id === action.payload.id);

      if (productInCart) {
        const product = { ...productInCart };
        product.quantity++;
        const products = [...state.products];
        const index = state.products.indexOf(productInCart);
        products[index] = product;
        const { totalPrice, totalProducts } = getTotals(products);
        return { ...state, products, totalPrice, totalProducts };
      } else {
        const product = { ...action.payload, quantity: 1 };
        const products = [...state.products, product];
        const { totalPrice, totalProducts } = getTotals(products);
        return { ...state, products, totalPrice, totalProducts };
      }
    }
    case cartActions.changeProductQuantity: {
      const productInCart = state.products.find((product) => product.id === action.payload.id);

      if (productInCart) {
        const product = { ...productInCart };
        product.quantity = action.payload.quantity;
        const products = [...state.products];
        const index = state.products.indexOf(productInCart);
        products[index] = product;
        const { totalPrice, totalProducts } = getTotals(products);
        return { ...state, products, totalPrice, totalProducts };
      } else {
        return state;
      }
    }
    case cartActions.clear:
      return { ...initialState };
    case cartActions.removeProduct: {
      const products = state.products.filter((product) => product.id !== action.payload);
      const { totalPrice, totalProducts } = getTotals(products);
      return { ...state, products, totalPrice, totalProducts };
    }
    default:
      return state;
  }
}
