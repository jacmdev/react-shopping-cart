import { useField } from 'formik';

export default function Checkbox(props) {
  const [field, meta] = useField({ ...props, type: 'checkbox' });
  const hasError = meta.touched && meta.error;

  return (
    <div className={`form-group ${hasError ? 'm--error' : ''}`}>
      <input id={props.name} className="form-choice-input" type="checkbox" {...field} />
      <label htmlFor={props.name}>
        <span className="form-choice m--checkbox"></span>
        {props.children}
      </label>
      {hasError && <div className="form-error">{meta.error}</div>}
    </div>
  );
}
