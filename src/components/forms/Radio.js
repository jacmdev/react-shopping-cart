import { useField } from 'formik';

export default function Radio(props) {
  const [field, meta] = useField(props);
  const hasError = meta.touched && meta.error;

  const changeHandler = (event) => {
    field.onChange(event);
    if (props.onChange) {
      props.onChange(event);
    }
  };

  const mapOptions = (option) => {
    const id = `${props.name}-${option.value}`;
    return (
      <div key={option.value}>
        <input
          className="form-choice-input"
          type="radio"
          {...field}
          id={id}
          value={option.value}
          checked={field.value === option.value}
          onChange={changeHandler}
        />
        <label htmlFor={id}>
          <span className="form-choice m--radio"></span>
          {option.label}
          {option.extra}
        </label>
      </div>
    );
  };

  return (
    <div className={`form-group ${hasError ? 'm--error' : ''}`}>
      {props.options.map(mapOptions)}
      {hasError && <div className="form-error">{meta.error}</div>}
    </div>
  );
}
