import { useState } from 'react';
import classes from './Quantity.module.css';

export default function Quantity({ max, min, onUpdate, value: defaultValue }) {
  const [value, setValue] = useState(defaultValue);

  const changeHandler = (event) => {
    setValue(event.target.value);
  };

  const decreaseHandler = () => {
    const newValue = +value - 1;
    if (newValue >= min) {
      setValue(newValue);
      onUpdate(newValue);
    }
  };

  const increaseHandler = () => {
    const newValue = +value + 1;
    if (newValue <= max) {
      setValue(newValue);
      onUpdate(newValue);
    }
  };

  const updateHandler = (event) => {
    let newValue = +event.target.value;

    if (newValue < min) {
      newValue = min;
      setValue(newValue);
    } else if (newValue > max) {
      newValue = max;
      setValue(newValue);
    }

    onUpdate(newValue);
  };

  return (
    <div className={classes.quantity}>
      <button className={classes.btn} type="button" disabled={value <= min} onClick={decreaseHandler}>
        -
      </button>
      <input className={classes.input} type="number" value={value} onBlur={updateHandler} onChange={changeHandler} />
      <button className={classes.btn} type="button" disabled={value >= max} onClick={increaseHandler}>
        +
      </button>
    </div>
  );
}
