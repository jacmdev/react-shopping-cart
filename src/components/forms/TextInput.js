import { useField } from 'formik';

export default function TextInput(props) {
  const [field, meta] = useField(props);
  const hasError = meta.touched && meta.error;

  return (
    <div className={`form-group ${hasError ? 'm--error' : ''}`}>
      <label className="form-label" htmlFor={props.id || props.name}>
        {props.label}
      </label>
      <input className="form-control" {...field} {...props} />
      {hasError && <div className="form-error">{meta.error}</div>}
    </div>
  );
}
