import { useSelector } from 'react-redux';
import checkoutConfig from '../../../config/checkout';
import CheckoutSummaryProduct from './CheckoutSummaryProduct';
import classes from './CheckoutSummary.module.css';

export default function CheckoutSummary() {
  const cartState = useSelector((state) => state.cart);
  const checkoutState = useSelector((state) => state.checkout);
  const paymentMethod = checkoutConfig.paymentMethods.find((payment) => payment.value === checkoutState.payment);
  const shippingMethod = checkoutConfig.shippingMethods.find((shipping) => shipping.value === checkoutState.shipping);

  return (
    <>
      <header className="header">
        <h1>Summary</h1>
      </header>
      <section className={classes.section}>
        <h2>Products</h2>
        <ul>
          {cartState.products.map((product) => (
            <CheckoutSummaryProduct key={product.id} {...product} />
          ))}
        </ul>
      </section>
      <section className={classes.section}>
        <h2>Shipping and payment</h2>
        <p>
          {checkoutState.name},<br />
          {checkoutState.address},<br />
          {checkoutState.phone}
        </p>
        <p>
          Shipping method: {shippingMethod.label}
          <br />
          Payment method: {paymentMethod.label}
        </p>
        {checkoutState.optional && <p>Optional</p>}
      </section>
    </>
  );
}
