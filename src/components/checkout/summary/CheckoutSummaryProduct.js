import Big from 'big.js';
import { currency } from '../../../utils';
import ProductImage from '../../product/ProductImage';
import classes from './CheckoutSummaryProduct.module.css';

export default function CheckoutSummaryProduct({ image, price, quantity, title }) {
  const productTotalPrice = new Big(price).times(quantity);

  return (
    <li className={classes.item}>
      <div className={classes.img}>
        <ProductImage image={image} size="sm" />
      </div>
      <div>
        {title}
        <div className={classes.details}>
          {currency(price, false)} &times; {quantity}
        </div>
      </div>
      <div className={classes['total-price']}>{currency(productTotalPrice)}</div>
    </li>
  );
}
