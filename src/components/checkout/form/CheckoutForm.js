import { Form, Formik, useFormikContext } from 'formik';
import { useContext, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import checkoutConfig from '../../../config/checkout';
import { CheckoutContext } from '../../../context/checkout-context';
import { checkoutActions } from '../../../store/checkout';
import { Checkbox, Radio, TextInput } from '../../forms';
import classes from './CheckoutForm.module.css';

export default function CheckoutForm() {
  const dispatch = useDispatch();
  const checkoutState = useSelector((state) => state.checkout);
  const { setNextStep, showInvalidFormMessage } = useContext(CheckoutContext);
  const initialValues = { ...checkoutState };

  const validationSchema = Yup.object({
    name: Yup.string().required('This field is required').max(32, 'Max length of this field is 32'),
    address: Yup.string().required('This field is required').max(32, 'Max length of this field is 32'),
    phone: Yup.string().required('This field is required').max(16, 'Max length of this field is 16'),
    shipping: Yup.string().required('Choose one option'),
    payment: Yup.string().required('Choose one option'),
  });

  const submitHandler = (values) => {
    dispatch({ type: checkoutActions.save, payload: { ...values, isDone: true } });
    setNextStep();
  };

  const shippingChangeHandler = (event) => {
    dispatch({ type: checkoutActions.updateShipping, payload: event.target.value });
  };

  const FormEffects = () => {
    const { submitCount, isValid } = useFormikContext();

    useEffect(() => {
      if (submitCount > 0) {
        showInvalidFormMessage(!isValid);
      }
    }, [isValid, submitCount]);

    return null;
  };

  return (
    <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={submitHandler}>
      <Form id="checkout-form" className={classes.form}>
        <FormEffects />
        <section className={classes.section}>
          <h2>Address</h2>
          <div className={classes['address-row']}>
            <TextInput label="Name" name="name" type="text" />
            <TextInput label="Address" name="address" type="text" />
            <TextInput label="Phone number" name="phone" type="text" />
          </div>
        </section>
        <section className={classes.section}>
          <h2>Shipping</h2>
          <Radio name="shipping" onChange={shippingChangeHandler} options={checkoutConfig.shippingMethods} />
        </section>
        <section className={classes.section}>
          <h2>Payment</h2>
          <Radio name="payment" options={checkoutConfig.paymentMethods} />
        </section>
        <section className={classes.section}>
          <h2>Extra</h2>
          <Checkbox name="optional">Optional</Checkbox>
        </section>
      </Form>
    </Formik>
  );
}
