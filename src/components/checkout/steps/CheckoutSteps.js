import { useContext } from 'react';
import checkoutConfig from '../../../config/checkout';
import { CheckoutContext } from '../../../context/checkout-context';
import classes from './CheckoutSteps.module.css';

export default function CheckoutSteps() {
  const { currentStep, changeStep } = useContext(CheckoutContext);

  return (
    <div className={classes.steps}>
      {checkoutConfig.stepsOrdered.map((step, index) => {
        const stepNumber = checkoutConfig.steps[step].id;
        let className = classes.step;

        if (currentStep === stepNumber) {
          className += ` ${classes['m--current']}`;
        } else if (currentStep > stepNumber) {
          className += ` ${classes['m--success']}`;
        }

        return (
          <button
            key={step}
            className={className}
            type="button"
            disabled={currentStep <= stepNumber}
            onClick={changeStep.bind(null, stepNumber)}
          >
            <span className={classes.number}>
              {currentStep > stepNumber ? <i className="fa fa-check"></i> : stepNumber}
            </span>
            <span className={classes.title}>{checkoutConfig.steps[step].name}</span>
          </button>
        );
      })}
    </div>
  );
}
