import { useSelector } from 'react-redux';
import CheckoutCartItem from './CheckoutCartItem';

export default function CheckoutCart() {
  const cartState = useSelector((state) => state.cart);

  return (
    <>
      <header className="header">
        <h1>Cart</h1>
      </header>
      <ul>
        {cartState.products.map((product) => (
          <CheckoutCartItem key={product.id} {...product} />
        ))}
      </ul>
    </>
  );
}
