import Big from 'big.js';
import { useDispatch } from 'react-redux';
import cartConfig from '../../../config/cart';
import { cartActions } from '../../../store/cart';
import { currency } from '../../../utils';
import { Quantity } from '../../forms';
import ProductImage from '../../product/ProductImage';
import classes from './CheckoutCartItem.module.css';

export default function CheckoutCartItem({ description, id, image, price, quantity, title }) {
  const dispatch = useDispatch();
  const totalPrice = new Big(price).times(quantity).toNumber();

  const quantityUpdateHandler = (newQuantity) => {
    dispatch({ type: cartActions.changeProductQuantity, payload: { id, quantity: newQuantity } });
  };

  const removeHandler = () => {
    dispatch({ type: cartActions.removeProduct, payload: id });
  };

  return (
    <div className={classes.item}>
      <div>
        <ProductImage image={image} />
      </div>
      <div className={classes.content}>
        <div className={classes.title}>{title}</div>
        <p className={classes.description}>{description}</p>
        <div className={classes.price}>{currency(price)}</div>
      </div>
      <div>
        <Quantity value={quantity} min={1} max={cartConfig.maxProductQuantity} onUpdate={quantityUpdateHandler} />
      </div>
      <div className={classes['total-price']}>{currency(totalPrice)}</div>
      <div>
        <button className={`btn m--primary ${classes['remove-btn-sm']}`} type="button" onClick={removeHandler}>
          Remove
        </button>
        <button className={classes['remove-btn-md']} type="button" onClick={removeHandler}>
          <i className="fa fa-times"></i>
        </button>
      </div>
    </div>
  );
}
