import Big from 'big.js';
import { useContext, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import checkoutConfig from '../../../config/checkout';
import { CheckoutContext } from '../../../context/checkout-context';
import { currency } from '../../../utils';
import classes from './CheckoutInfo.module.css';

export default function CheckoutInfo() {
  const cartState = useSelector((state) => state.cart);
  const checkoutState = useSelector((state) => state.checkout);
  const { currentStep, isIvalidFormMessageVisible, setNextStep } = useContext(CheckoutContext);
  const history = useHistory();
  const [isBtnDisabled, setIsBtnDisabled] = useState(false);
  const shippingMethod = checkoutConfig.shippingMethods.find((shipping) => shipping.value === checkoutState.shipping);
  const shippingCost = shippingMethod ? shippingMethod.cost : 0;
  const total = new Big(cartState.totalPrice).plus(shippingCost);

  const clickHandler = () => {
    if (currentStep === checkoutConfig.steps.summary.id) {
      history.push('/confirmation');
      return;
    }

    if (currentStep !== checkoutConfig.steps.form.id) {
      setNextStep();
    }

    setTimeout(() => {
      setIsBtnDisabled(true);
    }, 0);
    setTimeout(() => {
      setIsBtnDisabled(false);
    }, 500);
  };

  return (
    <div className={classes.info}>
      <h3 className={classes.heading}>Order info</h3>
      <div className={classes.items}>
        <div className={classes.item}>
          <div>Subtotal</div>
          <div className={classes.value}>{currency(cartState.totalPrice, false)}</div>
        </div>
        <div className={classes.item}>
          <div>Shipping cost</div>
          <div className={classes.value}>{shippingCost === 0 ? 'Free' : currency(shippingCost, false)}</div>
        </div>
        <div className={`${classes.item} ${classes.total}`}>
          <div>Total</div>
          <div className={classes.value}>{currency(total)}</div>
        </div>
      </div>
      {isIvalidFormMessageVisible && <div className={classes['form-errors-message']}>Form contains errors</div>}
      {currentStep === checkoutConfig.steps.form.id && (
        <button
          className="btn m--primary m--full m--md"
          type="submit"
          form="checkout-form"
          disabled={isBtnDisabled}
          onClick={clickHandler}
        >
          Continue
        </button>
      )}
      {currentStep !== checkoutConfig.steps.form.id && (
        <button className="btn m--primary m--full m--md" type="button" disabled={isBtnDisabled} onClick={clickHandler}>
          {currentStep === checkoutConfig.steps.summary.id ? 'Confirm and pay' : 'Continue'}
        </button>
      )}
    </div>
  );
}
