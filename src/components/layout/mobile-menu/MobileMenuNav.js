import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import Backdrop from '../../ui/backdrop/Backdrop';
import classes from './MobileMenuNav.module.css';

export default function MobileMenuNav({ isVisible, menuItems, onClose }) {
  const className = `${classes.menu} ${isVisible ? classes['m--visible'] : ''}`;

  return ReactDOM.createPortal(
    <>
      {isVisible && <Backdrop className={classes.backdrop} isUnderNavbar onClick={onClose} />}
      <div className={className}>
        <ul className={classes.nav}>
          {menuItems.map((item) => (
            <li key={item.label} className={classes.item}>
              <Link className={classes.link} to={item.link} onClick={onClose}>
                {item.label}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </>,
    document.getElementById('mobile-menu-nav')
  );
}
