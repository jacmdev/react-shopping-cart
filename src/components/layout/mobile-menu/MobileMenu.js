import { useState } from 'react';
import MobileMenuNav from '../mobile-menu/MobileMenuNav';
import classes from './MobileMenu.module.css';

export default function MobileMenu({menuItems}) {
  const [isMenuVisible, setIsMenuVisible] = useState(false);

  const closeHandler = () => {
    setIsMenuVisible(false);
  };

  const toggleHandler = () => {
    setIsMenuVisible((prev) => !prev);
  };

  return (
    <>
      <button className={classes.btn} type="button" onClick={toggleHandler}>
        {!isMenuVisible && <i className="fa fa-bars"></i>}
        {isMenuVisible && <i className="fa fa-times"></i>}
      </button>
      <MobileMenuNav isVisible={isMenuVisible} menuItems={menuItems} onClose={closeHandler} />
    </>
  );
}
