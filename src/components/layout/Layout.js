import classes from './Layout.module.css';
import Navbar from './navbar/Navbar';

export default function Layout(props) {
  return (
    <>
      <Navbar />
      <div className={'container ' + classes.container}>{props.children}</div>
    </>
  );
}
