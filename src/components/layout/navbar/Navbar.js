import { Link } from 'react-router-dom';
import MobileMenu from '../mobile-menu/MobileMenu';
import NavbarCart from './cart/NavbarCart';
import classes from './Navbar.module.css';

export default function Navbar() {
  const menuItems = [
    { label: 'Fruit', link: '/category-1' },
    { label: 'Bakery', link: '/category-2' },
  ];

  return (
    <div className={classes.container}>
      <nav className={`container ${classes.navbar}`}>
        <Link className={classes.brand} to="/">
          Shopping Cart
        </Link>
        <ul className={classes.nav}>
          {menuItems.map((item) => (
            <li key={item.label} className={classes.item}>
              <Link className={classes.link} to={item.link}>
                {item.label}
              </Link>
            </li>
          ))}
        </ul>
        <div className={classes.right}>
          <NavbarCart />
          <MobileMenu menuItems={menuItems} />
        </div>
      </nav>
    </div>
  );
}
