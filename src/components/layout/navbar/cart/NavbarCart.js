import { useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import NavbarCartInfo from './NavbarCartInfo';
import classes from './NavbarCart.module.css';

export default function NavbarCartBtn() {
  const [isInfoVisible, setIsInfoVisible] = useState(false);
  const cartState = useSelector((state) => state.cart);
  let infoVisibilityTimeout;

  function hideHandler(_, delay = 300) {
    infoVisibilityTimeout = setTimeout(() => {
      setIsInfoVisible(false);
    }, delay);
  }

  function showHandler() {
    clearTimeout(infoVisibilityTimeout);
    setIsInfoVisible(true);
  }

  return (
    <>
      <Link
        className={classes.btn}
        to="/checkout"
        onMouseEnter={showHandler}
        onMouseLeave={hideHandler}
        onClick={hideHandler.bind(null, null, 0)}
      >
        <i className="fa fa-shopping-cart"></i>
        {cartState.totalProducts > 99 && <div className={`${classes.count} ${classes['m--more']}`}>99+</div>}
        {cartState.totalProducts <= 99 && <div className={classes.count}>{cartState.totalProducts}</div>}
      </Link>
      <NavbarCartInfo
        isVisible={isInfoVisible}
        onMouseEnter={showHandler}
        onMouseLeave={hideHandler}
        onHide={hideHandler}
      />
    </>
  );
}
