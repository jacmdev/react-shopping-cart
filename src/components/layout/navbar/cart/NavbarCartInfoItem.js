import Big from 'big.js';
import { useDispatch } from 'react-redux';
import { cartActions } from '../../../../store/cart';
import { currency } from '../../../../utils';
import ProductImage from '../../../product/ProductImage';
import classes from './NavbarCartInfoItem.module.css';

export default function NavbarCartInfoItem({ id, image, price, quantity, title }) {
  const productTotalPrice = new Big(price).times(quantity);
  const dispatch = useDispatch();

  const removeHandler = () => {
    dispatch({ type: cartActions.removeProduct, payload: id });
  };

  return (
    <li className={classes.item}>
      <div className={classes.img}>
        <ProductImage image={image} size="sm" />
      </div>
      <div>
        {title}
        <div className={classes.details}>
          {currency(price, false)} &times; {quantity}
        </div>
      </div>
      <div className={classes['total-price']}>{currency(productTotalPrice)}</div>
      <div>
        <button className={classes['remove-btn']} type="button" onClick={removeHandler}>
          <i className="fa fa-times"></i>
        </button>
      </div>
    </li>
  );
}
