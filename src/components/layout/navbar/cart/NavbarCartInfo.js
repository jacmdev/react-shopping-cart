import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { currency } from '../../../../utils';
import NavbarCartInfoItem from './NavbarCartInfoItem';
import classes from './NavbarCartInfo.module.css';

export default function NavbarCartInfo({ isVisible, onHide, onMouseEnter, onMouseLeave }) {
  const className = `${classes.info} ${isVisible ? classes['m--visible'] : ''}`;
  const cartState = useSelector((state) => state.cart);

  return (
    <div className={className} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
      <div className={classes.header}>
        <div>Your cart</div>
        <div className={classes['header-total-price']}>{currency(cartState.totalPrice)}</div>
      </div>
      {cartState.products.length === 0 && <div className="text-center">Your cart is empty</div>}
      {cartState.products.length > 0 && (
        <ul className={classes.items}>
          {cartState.products.map((product) => (
            <NavbarCartInfoItem key={product.id} {...product} />
          ))}
        </ul>
      )}
      <footer className={classes.footer}>
        <Link to="/checkout" className="btn m--primary" onClick={onHide.bind(null, null, 0)}>
          Go to checkout
        </Link>
      </footer>
    </div>
  );
}
