import ProductImage from './ProductImage';
import { currency } from '../../utils';
import classes from './ProductInModal.module.css';

export default function ProductInModal({ description, image, price, title }) {
  return (
    <div className={classes.product}>
      <ProductImage image={image} />
      <div className={classes.content}>
        <div className={classes.title}>{title}</div>
        <p className={classes.description}>{description}</p>
        <div className={classes.price}>{currency(price)}</div>
      </div>
    </div>
  );
}
