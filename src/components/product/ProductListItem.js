import { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cartConfig from '../../config/cart';
import { ModalContext } from '../../context/modal-context';
import { cartActions } from '../../store/cart';
import { currency } from '../../utils';
import ProductInModal from './ProductInModal';
import ProductImage from './ProductImage';
import classes from './ProductListItem.module.css';

export default function ProductListItem({ description, id, image, price, title }) {
  const dispatch = useDispatch();
  const modalCtx = useContext(ModalContext);
  const cartState = useSelector((state) => state.cart);
  const productInCart = cartState.products.find((product) => product.id === id);
  const quantityInCart = productInCart ? productInCart.quantity : 0;
  const isQuantityMax = quantityInCart >= cartConfig.maxProductQuantity;

  const addToCartHandler = () => {
    const product = { description, id, image, price, title };
    dispatch({ type: cartActions.addProduct, payload: product });
    modalCtx.show({
      title: 'Product has been added to your cart',
      body: <ProductInModal {...product} />,
    });
  };

  return (
    <li className={classes.item}>
      <ProductImage image={image} size="product-list-item" />
      <div className={classes.content}>
        <div className={classes.title}>{title}</div>
        <p className={classes.description}>{description}</p>
      </div>
      <div className={classes.footer}>
        <div className={classes.price}>{currency(price)}</div>
        <button
          className={`btn m--primary ${classes['add-btn']} ${isQuantityMax ? 'm--disabled' : ''}`}
          type="button"
          disabled={isQuantityMax}
          onClick={addToCartHandler}
        >
          Add to cart
        </button>
      </div>
    </li>
  );
}
