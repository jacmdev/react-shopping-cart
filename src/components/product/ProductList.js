import Modal from '../ui/modal/Modal';
import ProductListItem from './ProductListItem';
import classes from './ProductList.module.css';

export default function ProductList({ products }) {
  return (
    <>
      <ul className={classes.list}>
        {products.map((product) => (
          <ProductListItem key={product.id} {...product} />
        ))}
      </ul>
      <Modal />
    </>
  );
}
