import classes from './ProductImage.module.css';

export default function ProductImage({ image, size }) {
  const className = `${classes['img']} ${size ? classes['m--' + size] : ''}`;
  return <div className={className} style={{ backgroundImage: `url(${image})` }}></div>;
}
