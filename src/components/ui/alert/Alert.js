import classes from './Alert.module.css';

export default function Alert({ children, type }) {
  let className = classes.alert;

  if (type) {
    className += ` ${classes['m--' + type]}`;
  }

  return <div className={className}>{children}</div>;
}
