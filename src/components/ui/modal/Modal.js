import React, { useContext } from 'react';
import ReactDOM from 'react-dom';
import { ModalContext } from '../../../context/modal-context';
import Backdrop from '../backdrop/Backdrop';
import classes from './Modal.module.css';

export default function Modal() {
  const modalCtx = useContext(ModalContext);
  const className = `${classes.modal} ${modalCtx.isVisible ? classes['m--visible'] : ''}`;

  const closeHandler = () => {
    modalCtx.hide();
  };

  return ReactDOM.createPortal(
    <>
      {modalCtx.isRendered && (
        <>
          <Backdrop onClick={closeHandler} />
          <div className={className}>
            {modalCtx.title && <div className={classes.header}>{modalCtx.title}</div>}
            {modalCtx.body && <div className={classes.body}>{modalCtx.body}</div>}
            <footer className={classes.footer}>
              <button className="btn m--primary" onClick={closeHandler}>
                Close
              </button>
            </footer>
          </div>
        </>
      )}
    </>,
    document.getElementById('modal')
  );
}
