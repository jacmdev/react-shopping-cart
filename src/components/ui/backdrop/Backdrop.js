import classes from './Backdrop.module.css';

export default function Backdrop(props) {
  const style = props.isUnderNavbar ? { zIndex: 18 } : null;
  const className = `${classes.backdrop} ${props.className}`;
  return <div className={className} style={style} onClick={props.onClick}></div>;
}
