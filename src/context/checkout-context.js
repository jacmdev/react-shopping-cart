import { createContext, useState } from 'react';

export const CheckoutContext = createContext({
  currentStep: 1,
  isIvalidFormMessageVisible: false,
  changeStep: () => undefined,
  setNextStep: () => undefined,
  showInvalidFormMessage: () => undefined,
});

export default function CheckoutContextProvider(props) {
  const [currentStep, setCurrentStep] = useState(1);
  const [isIvalidFormMessageVisible, setIsIvalidFormMessageVisible] = useState(false);

  const invalidFormMessageHandler = (isVisible) => {
    setIsIvalidFormMessageVisible(isVisible);
  };

  const nextStepHandler = () => {
    stepChangeHandler(currentStep + 1);
  };

  const stepChangeHandler = (step) => {
    setCurrentStep(step);
    setIsIvalidFormMessageVisible(false);
    window.scrollTo(0, 0);
  };

  return (
    <CheckoutContext.Provider
      value={{
        currentStep,
        isIvalidFormMessageVisible,
        changeStep: stepChangeHandler,
        setNextStep: nextStepHandler,
        showInvalidFormMessage: invalidFormMessageHandler,
      }}
    >
      {props.children}
    </CheckoutContext.Provider>
  );
}
