import { createContext, useState } from 'react';

export const ModalContext = createContext({
  body: '',
  isRendered: false,
  isVisible: false,
  title: '',
  hide: () => undefined,
  show: () => undefined,
});

export default function ModalContextProvider({ children }) {
  const [body, setBody] = useState('');
  const [isRendered, setIsRendered] = useState(false);
  const [isVisible, setIsVisible] = useState('');
  const [title, setTitle] = useState('');

  const hideHandler = () => {
    setIsVisible(false);
    setTimeout(() => {
      setIsRendered(false);
    }, 200);
  };

  const showHandler = (data) => {
    setTitle(data.title);
    setBody(data.body);
    setIsRendered(true);
    setTimeout(() => {
      setIsVisible(true);
    }, 0);
  };

  return (
    <ModalContext.Provider value={{ body, isVisible, isRendered, title, hide: hideHandler, show: showHandler }}>
      {children}
    </ModalContext.Provider>
  );
}
