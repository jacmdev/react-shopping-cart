import { useCallback, useState } from 'react';

export default function useHttp() {
  const [data, setData] = useState();
  const [error, setError] = useState();
  const [isLoaded, setIsLoaded] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const request = (url, options) => {
    setIsLoaded(false);
    setIsLoading(true);
    fetch(url, {
      headers: { 'Content-Type': 'application/json' },
      ...options,
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`${response.status} ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        setData(data);
        setIsLoading(false);
        setIsLoaded(true);
      })
      .catch((error) => {
        setError(error.message);
        setIsLoading(false);
        setIsLoaded(true);
      });
  };

  const getHandler = useCallback((url) => {
    request(url, { method: 'GET' });
  }, []);

  const postHandler = useCallback((url, body) => {
    request(url, { method: 'POST', body });
  }, []);

  return {
    data,
    error,
    isLoaded,
    isLoading,
    get: getHandler,
    post: postHandler,
  };
}
