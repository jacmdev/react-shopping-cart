import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import ProductList from '../components/product/ProductList';
import Loader from '../components/ui/loader/Loader';
import useHttp from '../hooks/use-http';

export default function Category() {
  const params = useParams();
  const [category, setCategory] = useState();
  const [products, setProducts] = useState([]);
  const { data: categoryData, isLoaded: isCategoryLoaded, isLoading: isCategoryLoading, get: getCategory } = useHttp();
  const { data: productsData, isLoaded: isProductsLoaded, isLoading: isProductsLoading, get: getProducts } = useHttp();
  const isNotCategory = params.id && isCategoryLoaded && !category;
  const isPageLoaded = (!params.id || (isCategoryLoaded && category)) && isProductsLoaded;

  useEffect(() => {
    if (params.id) {
      getCategory(`${process.env.REACT_APP_API_URL}/categories/${params.id}`);
    } else {
      setCategory({
        title: 'All products',
        description: `Lorem ipsum, dolor sit amet consectetur adipisicing elit. Animi architecto amet doloribus 
        voluptates? Vitae totam adipisci, iste hic ducimus nobis dignissimos ratione, cupiditate sequi iure animi 
        voluptatum consequatur officia dolores officiis asperiores, sint dolorem exercitationem in eaque architecto? 
        Earum, doloribus.`,
      });
      getProducts(`${process.env.REACT_APP_API_URL}/products`);
    }
  }, [params.id, getCategory, getProducts]);

  useEffect(() => {
    if (categoryData && categoryData.id) {
      setCategory(categoryData);
      getProducts(`${process.env.REACT_APP_API_URL}/products?category_id=${categoryData.id}`);
    }
  }, [categoryData, getProducts]);

  useEffect(() => {
    if (productsData) {
      setProducts(productsData);
    }
  }, [productsData]);

  return (
    <>
      {(isCategoryLoading || isProductsLoading) && <Loader />}
      {isNotCategory && <div className="text-center">Category not found</div>}
      {isPageLoaded && (
        <>
          <header className="header">
            <h1>{category.title}</h1>
            <p>{category.description}</p>
          </header>
          {products.length > 0 && <ProductList products={products} />}
          {products.length === 0 && <div className="text-center">No products found in this category</div>}
        </>
      )}
    </>
  );
}
