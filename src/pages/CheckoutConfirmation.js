import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import Alert from '../components/ui/alert/Alert';
import Loader from '../components/ui/loader/Loader';
import useHttp from '../hooks/use-http';
import { cartActions } from '../store/cart';
import { checkoutActions } from '../store/checkout';
import classes from './CheckoutConfirmation.module.css';

export default function CheckoutConfirmation() {
  const checkoutState = useSelector((state) => state.checkout);
  const dispatch = useDispatch();
  const history = useHistory();
  const { data, error, isLoaded, isLoading, post } = useHttp();

  useEffect(() => {
    if (checkoutState.isDone) {
      const body = JSON.stringify(checkoutState);
      post(`${process.env.REACT_APP_API_URL}/orders`, body);
    }
  }, [post, checkoutState, history]);

  useEffect(() => {
    if (data) {
      dispatch({ type: cartActions.clear });
      dispatch({ type: checkoutActions.clear });
    }
  }, [data, dispatch]);

  return (
    <>
      {isLoading && <Loader />}
      {isLoaded && error && <Alert type="danger">{error}</Alert>}
      {isLoaded && data && (
        <div className="text-center">
          <p>
            <i className={`fa fa-check-circle ${classes.icon}`}></i>
          </p>
          <p>Your order has been successfully processed</p>
          <p className={classes.thank}>Thank you!</p>
          <p className={classes.action}>
            <Link className="btn m--primary m--md" to="/">
              Continue shopping
            </Link>
          </p>
        </div>
      )}
    </>
  );
}
