export default function PageNotFound() {
  return <div className="text-center">Page not found</div>;
}
