import { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import CheckoutCart from '../components/checkout/cart/CheckoutCart';
import CheckoutForm from '../components/checkout/form/CheckoutForm';
import CheckoutInfo from '../components/checkout/info/CheckoutInfo';
import CheckoutSteps from '../components/checkout/steps/CheckoutSteps';
import CheckoutSummary from '../components/checkout/summary/CheckoutSummary';
import checkoutConfig from '../config/checkout';
import { CheckoutContext } from '../context/checkout-context';
import classes from './Checkout.module.css';

export default function Checkout() {
  const cartState = useSelector((state) => state.cart);
  const { currentStep } = useContext(CheckoutContext);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      {cartState.products.length === 0 && (
        <div className="text-center">
          <p className={classes['empty-icon']}>
            <i className="fa fa-shopping-cart"></i>
          </p>
          <p>Your cart is empty</p>
          <p className={classes['empty-action']}>
            <Link className="btn m--primary m--md" to="/">
              Continue shopping
            </Link>
          </p>
        </div>
      )}
      {cartState.products.length > 0 && (
        <div className={classes.checkout}>
          <div className={classes.content}>
            <CheckoutSteps />
            {currentStep === checkoutConfig.steps.cart.id && <CheckoutCart />}
            {currentStep === checkoutConfig.steps.form.id && <CheckoutForm />}
            {currentStep === checkoutConfig.steps.summary.id && <CheckoutSummary />}
          </div>
          <div className={classes.info}>
            <CheckoutInfo />
          </div>
        </div>
      )}
    </>
  );
}
