export function currency(value, isSignVisible = true) {
  return `${isSignVisible ? 'EUR ' : ''}${value.toFixed(2)}`;
}
